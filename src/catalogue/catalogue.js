import React, { PureComponent } from 'react';

import pick from 'lodash/pick';

import Genders, { gendersList } from './components/genders';
import Sortings, { sortingsList } from './components/sortings';
import ItemsList from './components/items-list';
import LoadMore from './components/load-more';

import { fetchItems } from './catalogue.api';
import styles from './catalogue.css';

const fetchFilters = ['gender', 'sorting', 'offset'];


class Catalogue extends PureComponent {
  state = {
    filtersWithShadow: false,

    gender: gendersList[0].value,
    sorting: sortingsList[0].value,

    items: [],
    itemsCount: -1,
    loading: false,
    offset: 0,
  };

  handleItemsFetching = ({ items, itemsCount }) => this.setState(
    state => ({
      itemsCount,
      items: state.offset === 0 ? items : state.items.concat(items),
      offset: state.offset + items.length,
      loading: false,
    }),
  );

  fetchItems = () => fetchItems(pick(this.state, fetchFilters))
    .then(this.handleItemsFetching);

  maybeFetchItems = () => this.state.loading || this.setState(
    { loading: true },
    this.fetchItems,
  );

  handleFiltersChange = filters => this.setState(
    { ...filters, offset: 0 },
    this.maybeFetchItems,
  );

  setGender = gender => () => this.handleFiltersChange({ gender });

  setSorting = ({ value }) => this.handleFiltersChange({ sorting: value });


  handleScroll = () => this.setState({ filtersWithShadow: window.pageYOffset > 99 });


  componentDidMount = () => {
    window.addEventListener('scroll', this.handleScroll);
    return this.fetchItems();
  };

  componentWillUnmount = () => (
    window.removeEventListener('scroll', this.handleScroll)
  );

  render = () => (
    <div className={styles.root}>
      <h1 className={styles.title}>FRONTEND TEST</h1>
      <div className={this.state.filtersWithShadow ? styles.filtersWithShadow : styles.filters}>
        <Genders
          active={this.state.gender}
          setGender={this.setGender}
        />

        <Sortings
          selected={this.state.sorting}
          setSorting={this.setSorting}
        />
      </div>

      <div className={styles.items}>
        <ItemsList items={this.state.items} />
      </div>
      {this.state.items.length < this.state.itemsCount && (
        <LoadMore
          loading={this.state.loading}
          onClick={this.maybeFetchItems}
        />
      )}
    </div>
  );
}

export default Catalogue;
