import React from 'react';
import PropTypes from 'prop-types';

import styles from './load-more.css';

const LoadMore = ({ loading, onClick }) => (
  loading
    ? 'Loading...'
    : (
      <button
        type="button"
        onClick={onClick}
        className={styles.root}
      >
        Load more
      </button>
    )
);

LoadMore.propTypes = {
  loading: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default LoadMore;
