import React, { memo } from 'react';
import PropTypes from 'prop-types';

import styles from './item.css';
import { item as itemType } from './item.types';

const Item = ({
  toggleLike,
  isLiked,
  item: {
    image, title, description, price, isNew,
  },
}) => (
  <div className={styles.root}>
    <div className={styles.imageArea}>
      <img src={image} alt={title} />
      <button
        type="button"
        onClick={toggleLike}
        className={isLiked ? styles.liked : styles.like}
      />
      {isNew && (
        <div className={styles.isNew}>
          <span>New</span>
        </div>
      )}
    </div>
    <div className={styles.data}>
      <div className={styles.info}>
        <div className={styles.title}>{title}</div>
        <div className={styles.description}>{description}</div>
      </div>
      <div className={styles.price}>
        {price}
        €
      </div>
    </div>
  </div>
);

Item.propTypes = {
  toggleLike: PropTypes.func.isRequired,
  isLiked: PropTypes.bool.isRequired,
  item: itemType.isRequired,
};

const arePropsEqual = (prevProps, nextProps) => (
  prevProps.isLiked === nextProps.isLiked
);

export default memo(
  Item,
  arePropsEqual,
);
