import PropTypes from 'prop-types';

export const item = PropTypes.shape({
  uid: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  isNew: PropTypes.bool.isRequired,
});
