export const getLSWishlist = () => (
  localStorage.getItem('wishlist')
    ? JSON.parse(localStorage.getItem('wishlist'))
    : []
);

export const setLSWishlist = wishlist => (
  localStorage.setItem('wishlist', JSON.stringify(wishlist))
);
