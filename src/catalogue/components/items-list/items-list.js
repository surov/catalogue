import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import xor from 'lodash/xor';

import Item from '../item';

import { getLSWishlist, setLSWishlist } from './items-list.lib';

import { item as itemType } from '../item/item.types';

class ItemsList extends PureComponent {
  static propTypes = {
    items: PropTypes.arrayOf(itemType).isRequired,
  }

  state = {
    wishlist: getLSWishlist(),
  }

  isLiked = itemUid => this.state.wishlist.includes(itemUid);

  toggleLike = itemUid => () => this.setState(
    state => ({ wishlist: xor(state.wishlist, [itemUid]) }),
    () => setLSWishlist(this.state.wishlist),
  );


  render() {
    return (
      this.props.items.map(item => (
        <Item
          key={item.uid}
          item={item}
          isLiked={this.isLiked(item.uid)}
          toggleLike={this.toggleLike(item.uid)}
        />
      ))
    );
  }
}

export default ItemsList;
