import React from 'react';
import PropTypes from 'prop-types';

import Select from 'react-select';

import styles from './sortings.css';


export const sortingsList = [
  { label: 'Price: low to high', value: 'plh' },
  { label: 'Price: high to low', value: 'phl' },
  { label: 'Rating: low to high', value: 'rlh' },
  { label: 'Rating: high to low', value: 'rhl' },
];

const getSortingByValue = val => sortingsList.find(({ value }) => value === val);


const Sortings = ({ selected, setSorting }) => (
  <div className={styles.root}>
    Sort by:
    <Select
      value={getSortingByValue(selected)}
      onChange={setSorting}
      options={sortingsList}
      className={styles.select}
      classNamePrefix="react-select"
      isSearchable={false}
    />
  </div>
);

Sortings.propTypes = {
  selected: PropTypes.string.isRequired,
  setSorting: PropTypes.func.isRequired,
};

export default Sortings;
