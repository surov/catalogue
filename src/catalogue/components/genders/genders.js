import React from 'react';
import PropTypes from 'prop-types';

import styles from './genders.css';

export const gendersList = [
  { label: 'Woman', value: 'w' },
  { label: 'Man', value: 'm' },
];

const isActive = (active, value) => active === value;

const Genders = ({ active, setGender }) => (
  <ul className={styles.root}>
    {gendersList.map(({ label, value }) => (
      <li
        key={value}
        className={isActive(active, value) ? styles.active : ''}
      >
        <label htmlFor={value}>
          <input
            type="radio"
            name="gender"
            id={value}
            value={value}
            checked={isActive(value)}
            onChange={setGender(value)}
          />
          {label}
        </label>
      </li>
    ))}
  </ul>
);

Genders.propTypes = {
  active: PropTypes.string.isRequired,
  setGender: PropTypes.func.isRequired,
};

export default Genders;
