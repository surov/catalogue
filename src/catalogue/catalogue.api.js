import sortBy from 'lodash/sortBy';

import { man, woman } from './items.json';


const manItemsByPriceASC = sortBy(man, 'price');
const manItemsByRatingASC = sortBy(man, 'rating');

const womanItemsByPriceASC = sortBy(woman, 'price');
const womanItemsByRatingASC = sortBy(woman, 'rating');

const sortedLists = {
  m: {
    plh: manItemsByPriceASC,
    phl: [...manItemsByPriceASC].reverse(),
    rlh: manItemsByRatingASC,
    rhl: [...manItemsByRatingASC].reverse(),
  },
  w: {
    plh: womanItemsByPriceASC,
    phl: [...womanItemsByPriceASC].reverse(),
    rlh: womanItemsByRatingASC,
    rhl: [...womanItemsByRatingASC].reverse(),
  },
};

const getSortedList = (gender, sorting) => sortedLists[gender][sorting];


export const fetchItems = ({ gender, sorting, offset }, limit = 9) => (
  Promise.resolve(getSortedList(gender, sorting))
    .then(items => ({
      itemsCount: items.length,
      items: items.slice(offset, offset + limit),
    }))
);
