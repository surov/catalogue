import React from 'react';
import { render } from 'react-dom';
import Catalogue from './catalogue';

import './index.css';

render(<Catalogue />, document.getElementById('root'));
