const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const path = require('path');

const withDirname = pth => path.join(__dirname, pth);


module.exports = {
  entry: {
    bundle: withDirname('/src/index.js'),
  },
  output: {
    path: withDirname('/build'),
    filename: '[name].js',
  },


  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: JSON.stringify({
          presets: ['react', 'env', 'stage-2'],
        }),
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            `css-loader?${JSON.stringify({
              modules: true,
              sourceMap: true,
              importLoaders: 1,
              localIdentName: '[name]__[local]___[hash:base64:5]',
            })}`,
            'postcss-loader',
          ],
        }),
      },
      {
        test: /\.woff2?$|.svg$/,
        loader: `file-loader?${JSON.stringify({
          name: 'assets/[name].[ext]',
        })}`,
      },
    ],
  },

  plugins: [
    new ExtractTextPlugin({ filename: 'styles.css', allChunks: true }),
    new HtmlWebpackPlugin({
      template: withDirname('/src/index.html'),
      inject: 'body',
    }),
  ],
};
